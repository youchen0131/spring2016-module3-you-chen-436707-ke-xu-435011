<?php
session_start();
require 'database.php';
date_default_timezone_set('America/Chicago');


$story_id=$_GET['story_id'];


if (isset($_SESSION['logged']) && ($_SESSION['logged']=1) 
		&& (isset($_SESSION['username']))) {
	$curr_user=$_SESSION['username'];
}

if (isset($_POST['comment'.$story_id]) && ($_SESSION['token'] == $_POST['token'])) {
        $content = $_POST["content"];
        $now = new DateTime();
        $create_date = $now->format("Y-m-d H:i:s");

        // Insert comment into the comments table
    	$stmt = $mysqli->prepare("INSERT INTO comments (comment_id, story_id, username, content, create_date) values ('', ?, ?, ?, ? )");
	    if(!$stmt) {
	        printf("Query Prep Failed: %s\n", $mysqli->error);
	        exit;
	    }
	     
	    $stmt->bind_param('isss', $story_id, $curr_user, $content, $create_date );
	    
	    if (!$stmt->execute()) {
	    	echo "mysql error begins:";
	        echo $mysqli->error;
	        echo "mysql error ends";
	        $stmt->close();
	    }
	    else {
	    	$stmt->close();
	    }

	    // Update number of comments in the stories table
	    $stmt = $mysqli->prepare("UPDATE stories SET number_of_comments = number_of_comments + 1 WHERE story_id = ?");
	    if(!$stmt) {
	        printf("Query Prep Failed: %s\n", $mysqli->error);
	        exit;
	    }
	     
	    $stmt->bind_param('i', $story_id );
	    
	    if (!$stmt->execute()) {
	    	echo "mysql error begins:";
	        echo $mysqli->error;
	        echo "mysql error ends";
	        $stmt->close();
	    }
	    else {
	    	$stmt->close();
	    }

    
}

?>


<!DOCTYPE html>
    <html>
        <head>
            <title>News Contents</title>
        </head>
        
        <body>
            <h1>News Contents</h1>
            <div>
        <?php                
            $stmt = $mysqli->prepare("SELECT title, content, category, link, create_date, number_of_comments, username FROM stories WHERE story_id=?");
            if(!$stmt) {
                printf("Query Prep Failed: %s\n at line 26", $mysqli->error);
                exit();
            }
            $stmt->bind_param('i', $story_id);
            $stmt->execute();
            if (!$stmt->execute()) {
                echo $mysqli->error;
                $stmt->close();
            }
            $stmt->bind_result($title, $content, $category, $link, $create_date, $number_of_comments, $username);
            $stmt->store_result();
            while ($stmt->fetch()) {
               
			    if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])) {
					if ($curr_user == $username ) {
                    $buttons = '';
					$buttons = $buttons.'<a href="editnews.php?story_id='.$story_id.'">Edit</a>'.'	'.
                    '<a href="deletestory.php?story_id='.$story_id.'">Delete</a><br>';
					echo $buttons;
					}						
						
						
                    $story = '';
                    $story = $story. '<h2>'.$title.'</h2>'. '<h3>Category: '.$category.'</h3>'.
                        '<div>'.$content.'</div><br>'.
                        '<div>'.
                        '<form action="'.htmlentities($link).'" method="POST">'.
                        '<textarea name="content" placeholder="Enter your comment here"></textarea><br>'.
                        '<input type="hidden" name="token" value="'.$_SESSION['token'].'" />'.
                        '<button type="submit" id="comment'.$story_id.'" name="comment'.$story_id.'">Comment</button></form>'.
                        '</div>';
                    echo $story;
					
					
					
                } else {
              	   $story = '';
                    $story = $story. '<h2>'.$title.'</h2>'.'<h3>Category: '.$category.'</h3>'.
                    	'<div>'.$content.'</div>';
                    echo $story;
                }
            
                ?>
                <br><br>
                <div>
                    <h3>Comments: </h3>
                <?php
                // Print out the comments
                $stmt2 = $mysqli->prepare("SELECT comment_id, story_id, username, content, create_date FROM comments WHERE story_id=?");
                if(!$stmt2){
                    printf("Query Prep Failed: %s\n at line 57", $mysqli->error);
                    exit();
                }
	 
                $stmt2->bind_param('i', $story_id);             
                $stmt2->execute();             
                $stmt2->bind_result($comment_id, $story_id, $username, $content, $create_date);
			
                while ($stmt2->fetch()) {
                    $comment = '';
                    $comment = $comment. 
                    '<div><p>'.$username.': '.$content.'<br>'.$create_date.'</p></div>';
                    echo $comment;
					if ($curr_user == $username ) {
                    $buttons = '';
					$buttons = $buttons.'<a href="editcomments.php?comment_id='.$comment_id.'">Edit</a>'.'	'.
                    '<a href="deletecomment.php?comment_id='.$comment_id.'">Delete</a><br>';
					echo $buttons;
					}

                }
                $stmt2->close();

            }
            $stmt->close();
        ?>
                </div>
            </div>
            <br><br>
            <div>
                <a href="index.php">BACK TO INDEX PAGE</a>
            </div>
        </body>
    </html>