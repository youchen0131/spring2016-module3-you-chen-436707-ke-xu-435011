<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.
 
session_start();
require 'database.php';


if(isset($_POST['login'])){
	$user=trim($_POST['username']);
	$password=trim($_POST['password']);

 
	// Use a prepared statement
	$stmt = $mysqli->prepare("SELECT COUNT(*), user_id, crypted_password FROM users WHERE username=?");
 
	// Bind the parameter
	$stmt->bind_param('s', $user);
	$stmt->execute();
	 
	// Bind the results
	$stmt->bind_result($cnt, $user_id, $pwd_hash);
	$stmt->fetch();
	 
	$pwd_guess = $_POST['password'];
	// Compare the submitted password to the actual password hash
	if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
		// Login succeeded!
		$_SESSION['user_id'] = $user_id;
		$_SESSION['username'] = $user;
		$_SESSION['logged'] = 1;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
		// Redirect to your target page
		header("Location: index.php");
	}else{
		// Login failed; redirect back to the login screen
		echo "Failed to log in. ";
	}
}
?>

<!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<meta charset="UTF-8">
</head>

<body>
	<form action="login.php" method="POST">
		<p>Enter your username and password to log in</p>
		<p>Username: </p>
		<input type="text" id="username" name="username" required>
		<p>Password: </p>
		<input type="password" id="password" name="password" required>
		<button type="submit" name="login">Login</button>
	</form>

</body>



</html>