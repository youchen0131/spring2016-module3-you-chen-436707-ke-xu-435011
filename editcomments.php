<?php
session_start();
require 'database.php';

if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])){
  $username = $_SESSION["username"];
  
    $comment_id=$_GET['comment_id'];
}

if (isset($_POST['edit']) && ($_SESSION['token'] == $_POST['token'])){
    $content = $_POST['content'];
    $stmt = $mysqli->prepare("UPDATE comments SET content = ? WHERE comment_id = ?");
	if(!$stmt) {
	    printf("Query Prep Failed: %s\n at line 15", $mysqli->error);
	    exit;
	}
	     
	$stmt->bind_param('si', $content, $comment_id );
	    
	if (!$stmt->execute()) {
	    echo "mysql error begins:";
	    echo $mysqli->error;
	    echo "mysql error ends";
	    $stmt->close();
	} else {
	    $stmt->close();
	}
    
    header ("Location: account.php");
    exit();


}
?>


<!DOCTYPE html>
    <html>
        <head>
            <title>Editting Comment</title>
        </head>
        <body>
            <h1>Edit Your Comment</h1>
            
            <?php
            $stmt = $mysqli->prepare("SELECT content FROM comments WHERE comment_id=?");
            if(!$stmt) {
                printf("Query Prep Failed: %s\n at line 49", $mysqli->error);
                exit();
            }
		
            $stmt->bind_param('i', $comment_id);
            $stmt->execute();
            $stmt->bind_result($content);
            $stmt->store_result();
            while ($stmt->fetch()) {
        
            ?>
            
            
            <div>
                <form action="editcomments.php?comment_id=<?php echo htmlentities($comment_id); ?>" method="POST">
                    <p><input type="text" name="content" value="<?php echo htmlentities($content); ?>"></p><br>
					<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                    <button type="submit" name="edit">Submit</button>
                </form>
            </div>
            
            <?php
            }
            ?>
        </body>
    </html>
    