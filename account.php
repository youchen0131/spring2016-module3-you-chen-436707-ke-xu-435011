<?php
session_start();
require 'database.php';

if (isset($_SESSION['logged']) && ($_SESSION['logged'] == 1) && isset($_SESSION['username'])) {
	$username = $_SESSION['username'];
}
else {
	header("Location: signup.php");
    exit();
}
?>


<!DOCTYPE html>
    <html>
        <head>
            <title>Manage My Account</title>
        </head>
        <body>
            <div>
				<h2><?php echo htmlentities($username); ?>'s Page</h2>
                <h1> -------- My Stories -------- </h1>
                <?php
                $stmt = $mysqli->prepare("SELECT story_id, title, content, category, link, create_date, number_of_comments FROM stories WHERE username = ?");
                if(!$stmt) {
                    printf("Query Pep Failed: %s\n", $mysqli->error);
                    exit();
                }
                $stmt->bind_param('s',$username);
                $stmt->execute();
                $stmt->bind_result($story_id, $title, $content, $category, $link, $create_date, $number_of_comments);
                
                $stories = '';
                $number = 1;
                
                while ($stmt->fetch()) {
					$story = '';
                    $story = $story. '<h2>'.$number.' --- '.$title.'</h2>'.'<h3>Category: '.$category.'</h3>'.
                        '<div>'.$content.'</div><br>'.
						//'<form action="editnews.php?story_id='.$story_id.'" method="GET">
						//<button>Edit</button></form>'.
						//'<form action="deletenews.php?story_id='.$story_id.'" method="GET">
						//<button>Delete</button></form>';


                    '<a href="editnews.php?story_id='.$story_id.'">Edit</a>'.'	'.
                    '<a href="deletestory.php?story_id='.$story_id.'">Delete</a><br>';
                    $number=$number+1;
                    $stories=$stories.$story;
                }
                echo $stories;
                $stmt->close();

                ?>
            </div>

            <div>
                <h1> -------- My Comments -------- </h1>
                <?php
                $stmt = $mysqli->prepare("SELECT stories.title, comments.comment_id, comments.content,
                                         comments.story_id, comments.create_date FROM comments JOIN stories
                                         ON (stories.story_id = comments.story_id AND comments.username=?)");
                if(!$stmt) {
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit();
                }
                
                $stmt->bind_param('s',$username);
                $stmt->execute();
                $stmt->bind_result($title, $comment_id, $content, $story_id, $create_date);
                
                $comments = '';
                $number = 1;
                
                while ($stmt->fetch()){
					$comment ='';
					$comment = $comment. '<h2>'.$number.' --- '.$title.'</h2>'.
                        '<div>'.$content.'</div><br>'.$create_date.
					'<br><a href="editcomments.php?comment_id='.$comment_id.'">Edit</a>'.'	'.
                    '<a href="deletecomment.php?comment_id='.$comment_id.'">Delete</a><br>';
                
                    $number=$number+1;
                    $comments=$comments.$comment;
                }
                
                echo $comments;
                $stmt->close();
                ?>
            </div>
			<br><br>
			<div>
                <a href="index.php">BACK TO INDEX PAGE</a>
            </div>

        </body>
    </html>