<?php
session_start();
require 'database.php';

if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])){
  $username = $_SESSION["username"];
  
    $story_id=$_GET['story_id'];
}


if (isset($_POST['delete']) && ($_SESSION['token'] == $_POST['token'])) {
$stmt = $mysqli->prepare("DELETE FROM comments WHERE story_id=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit();
}
 
$stmt->bind_param('i', $story_id);             
$stmt->execute();    
$stmt->close();

$stmt = $mysqli->prepare("DELETE FROM stories WHERE story_id=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit();
}
	 
$stmt->bind_param('i', $story_id);             
$stmt->execute();   
$stmt->close();          

header("Location: account.php");
exit();
}


?>


<!DOCTYPE html>
  <html>
	<head>
	  <title>DELETE STORY</title>
	</head>
	<body>
	  <p>Delete this news?</p>
	  <form action="deletestory.php?<?php echo 'story_id='.$story_id ?>" method="POST">
		<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
        <button type="submit" name="delete">Yes</button>
	  </form>
	</body>
  </html>