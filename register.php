<?php

session_start();
require 'database.php';


if(isset($_POST['register']) && ($_SESSION['token'] == $_POST['token'])){
	$username = trim($_POST['username']);
	$password = trim($_POST['password']);
	if ( !preg_match('[\w\b]', $username) ){
		echo "Invalid username";
		exit;
	}

	
	
	$pwd_hash = crypt($password);
	
	$stmt = $mysqli->prepare("INSERT INTO users (user_id, username, crypted_password) VALUES ('', ?, ?)");
	if(!$stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit;
	}
	
	$stmt->bind_param("ss", $username, $pwd_hash);
	
	if (!$stmt->execute()){
		echo "Username already exists. ";
		$stmt->close();
		session_destroy();
	} else {
		echo "User ".$username. " registered successfully";
		$stmt->close();
		header("Location: login.php");
		exit();
	}
}

?>

<!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<meta charset="UTF-8">
</head>

<body>
	<form action="register.php" method="POST">
		<p>Create your username and password</p>
		<p>Username: </p>
		<input type="text" id="username" name="username" required>
		<p>Password: </p>
		<input type="password" id="password" name="password" required>
		<button type="submit" name="register">Register</button>
	</form>

</body>



</html>