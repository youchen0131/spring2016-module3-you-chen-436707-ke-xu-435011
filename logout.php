<?php 
session_start();

if(isset($_SESSION['logged']) && $_SESSION['logged'] ==1 && isset($_SESSION['username'])) {

	session_destroy();
	header("Location: index.php");

}else {
    header("Location: index.php");
}
?>