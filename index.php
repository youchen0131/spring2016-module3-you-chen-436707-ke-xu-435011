<?php
session_start();
require 'database.php';

date_default_timezone_set('America/Chicago');

if (isset($_SESSION['logged']) && ($_SESSION['logged']=1) 
		&& (isset($_SESSION['username']))) {
	$username=$_SESSION['username'];
}

if (isset($_POST['upload']) && ($_SESSION['token'] == $_POST['token'])){
	$title = $_POST["title"];
	$content = $_POST["content"];
	$category = $_POST['category'];
	$now = new DateTime();
	$create_date = $now->format("Y-m-d H:i:s");
	
	$stmt=$mysqli->prepare("INSERT INTO stories (story_id, username, title, content, create_date, category) VALUES ('', ?, ?, ?, ?, ?)");
	if(!$stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit();
	}
	
	$stmt->bind_param('sssss',$username, $title, $content, $create_date, $category);
	
	if (!$stmt->execute()) {
	    echo $mysqli->error;
	    $stmt->close();
	} else {		
		$stmt->prepare("UPDATE stories SET link=? WHERE story_id=?");
		if(!$stmt) {
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit();
		}
		
		$lastid=$mysqli->insert_id;
		
		$link="newscontent.php?story_id=".$lastid;
		$stmt->bind_param('si',$link, $lastid);
		
		
		$stmt->execute();
		if (!$stmt->execute()) {
			echo $mysqli->error;
			$stmt->close();
		} else {
			$stmt->close();
		}
	}
}

if(isset($_POST['choose_category'])) {
	$news_category=$_POST['chosen_category'];
}




?>

<!DOCTYPE html>
<html>
<head>
	<title>News</title>
</head>

<body>
	<div>
	<?php if (isset($_SESSION['logged']) && ($_SESSION['logged']=1) 
						&& (isset($_SESSION['username']))) { ?>
		<ul>
			<li>Welcome, <?php echo htmlentities($username); ?></li>
			<li><a href="account.php">My Account</a></li>
			<li><a href="logout.php">Log Out</a></li>
		</ul>
	<?php } else { ?>
	
		<ul>
			<li>Welcome, guest</li>
			<li><a href="login.php">Log In</a></li>
			<li><a href="register.php">Register</a></li>
		</ul>
	<?php } ?>				
	</div>	

	<div>
	<?php if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])) { ?>
			<p>Upload a story: </p>
			<form action='index.php' method="POST">
				<div>
				<input type="text" name="title" placeholder="News Title" required>
				<input type="text" name="content" placeholder="News Content" required>
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
				<select name="category">
					<option value="Technology">Technology</option>
					<option value="News">News</option>
					<option value="Entertainment">Entertainment</option>
					<option value="Sports">Sports</option>
				</select>
				</div>
				<button type="submit" name="upload">Upload</button>
			</form>
	<?php } ?>
	</div>
	
	
	<div>
		<h1>Stories</h1>
		<div>
		<p>See only:</p>
		<form action="index.php" method="POST">
			<select name="chosen_category">
				<option <?php if( (!isset($news_category)) || $news_category=="All") { echo "selected='selected'";} ?> value="All">All</option>
				<option <?php if( (isset($news_category)) && $news_category=="Technology") { echo "selected='selected'";} ?> value="Technology">Technology</option>
				<option <?php if( (isset($news_category)) && $news_category=="News") { echo "selected='selected'";} ?> value="News">News</option>
				<option <?php if( (isset($news_category)) && $news_category=="Entertainment") { echo "selected='selected'";} ?> value="Entertainment">Entertainment</option>
				<option <?php if( (isset($news_category)) && $news_category=="Sports") { echo "selected='selected'";} ?> value="Sports">Sports</option>
			</select>
			<button type="submit" name="choose_category">OK</button>
		</form>
		</div>
		<br><br>
		
		
		<?php
		
		
		if( (!isset($news_category)) || $news_category=="All") {
		
			// Print out the stories
			$stmt = $mysqli->prepare("SELECT story_id, title, content, category, create_date, number_of_comments, link, username FROM stories");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($story_id, $title, $content, $category, $create_date, $number_of_comments, $link, $username);
			$stmt->store_result();
			while ($stmt->fetch()) {
					$story = '';
					$story = $story. '<a href="'.$link.'"><h2>'.$title.'</h2></a>'.'<h3>Category: '.$category.'</h3>'.
						'<div>'.$content.'</div>';
					echo $story;
			}
			$stmt->close();
			
		} else if ($news_category=="Technology") {
			// Print out the stories
			$stmt = $mysqli->prepare("SELECT story_id, title, content, category, create_date, number_of_comments, link, username FROM stories WHERE category='Technology'");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($story_id, $title, $content, $category, $create_date, $number_of_comments, $link, $username);
			$stmt->store_result();
			while ($stmt->fetch()) {
					$story = '';
					$story = $story. '<a href="'.$link.'"><h2>'.$title.'</h2></a>'.'<h3>Category: '.$category.'</h3>'.
						'<div>'.$content.'</div>';
					echo $story;
			}
			$stmt->close();
		} else if ($news_category=="News") {
			// Print out the stories
			$stmt = $mysqli->prepare("SELECT story_id, title, content, category, create_date, number_of_comments, link, username FROM stories WHERE category='News'");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($story_id, $title, $content, $category, $create_date, $number_of_comments, $link, $username);
			$stmt->store_result();
			while ($stmt->fetch()) {
					$story = '';
					$story = $story. '<a href="'.$link.'"><h2>'.$title.'</h2></a>'.'<h3>Category: '.$category.'</h3>'.
						'<div>'.$content.'</div>';
					echo $story;
			}
			$stmt->close();
		} else if ($news_category=="Entertainment") {
			// Print out the stories
			$stmt = $mysqli->prepare("SELECT story_id, title, content, category, create_date, number_of_comments, link, username FROM stories WHERE category='Entertainment'");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($story_id, $title, $content, $category, $create_date, $number_of_comments, $link, $username);
			$stmt->store_result();
			while ($stmt->fetch()) {
					$story = '';
					$story = $story. '<a href="'.$link.'"><h2>'.$title.'</h2></a>'.'<h3>Category: '.$category.'</h3>'.
						'<div>'.$content.'</div>';
					echo $story;
			}
			$stmt->close();
		} else if ($news_category=="Sports") {
			// Print out the stories
			$stmt = $mysqli->prepare("SELECT story_id, title, content, category, create_date, number_of_comments, link, username FROM stories WHERE category='Sports'");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($story_id, $title, $content, $category, $create_date, $number_of_comments, $link, $username);
			$stmt->store_result();
			while ($stmt->fetch()) {
					$story = '';
					$story = $story. '<a href="'.$link.'"><h2>'.$title.'</h2></a>'.'<h3>Category: '.$category.'</h3>'.
						'<div>'.$content.'</div>';
					echo $story;
			}
			$stmt->close();
		}
		
		?>
	</div>
</body>
</html>