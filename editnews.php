<?php
session_start();
require 'database.php';

if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])){
  $username = $_SESSION["username"];
  
    $story_id=$_GET['story_id'];
}

if (isset($_POST['edit']) && ($_SESSION['token'] == $_POST['token'])){
    $title = $_POST["title"];
    $content = $_POST['content'];
	$category = $_POST['category'];
    $stmt = $mysqli->prepare("UPDATE stories SET title = ?, content = ?, category = ? WHERE story_id = ?");
	if(!$stmt) {
	    printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit;
	}
	     
	$stmt->bind_param('sssi', $title, $content, $category, $story_id );
	    
	if (!$stmt->execute()) {
	    echo "mysql error begins:";
	    echo $mysqli->error;
	    echo "mysql error ends";
	    $stmt->close();
	} else {
	    $stmt->close();
	}
    
    header ("Location: account.php");
    exit();


}
?>


<!DOCTYPE html>
    <html>
        <head>
            <title>Editting News</title>
        </head>
        <body>
            <h1>Edit Your News</h1>
            
            <?php
            $stmt = $mysqli->prepare("SELECT title, content, category, create_date, number_of_comments, username FROM stories WHERE story_id=?");
            if(!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit();
            }
		
            $stmt->bind_param('i', $story_id);
            $stmt->execute();
            $stmt->bind_result($title, $content, $category, $create_date, $number_of_comments, $username);
            $stmt->store_result();
            while ($stmt->fetch()) {
        
            ?>
            
            
            <div>
                <form action="editnews.php?story_id=<?php echo htmlentities($story_id); ?>" method="POST">
                    <p><input type="text" name="title" value="<?php echo htmlentities($title); ?>" required></p>
<!--                    <p><input type="text" name="content" value="<?php echo htmlentities($content); ?>"></p>
-->                    <p><textarea name="content"><?php echo htmlentities($content); ?></textarea></p>
					<div>
					<select name="category">
					  <option <?php if ($category=="Technology") { echo "selected='selected'";} ?> value="Technology">Technology</option>
					  <option <?php if ($category=="News") { echo "selected='selected'";} ?> value="News">News</option>
					  <option <?php if ($category=="Entertainment") { echo "selected='selected'";} ?> value="Entertainment">Entertainment</option>
					  <option <?php if ($category=="Sports") { echo "selected='selected'";} ?> value="Sports">Sports</option>
					</select>
					</div> 
					<br><br>
					<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                    <button type="submit" name="edit">Submit</button>
                </form>
            </div>
            
            <?php
            }
            ?>
        </body>
    </html>
    