<?php
session_start();
require 'database.php';

if (isset($_SESSION["logged"]) && ($_SESSION["logged"] == 1) && isset($_SESSION["username"])){
  $username = $_SESSION["username"];
  
    $comment_id=$_GET['comment_id'];
}


if (isset($_POST['delete']) && ($_SESSION['token'] == $_POST['token'])) {


$stmt = $mysqli->prepare("DELETE FROM comments WHERE comment_id=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit();
}
 
$stmt->bind_param('i', $comment_id);             
$stmt->execute();    
$stmt->close();



header("Location: account.php");
exit();
}
?>


<!DOCTYPE html>
  <html>
	<head>
	  <title>DELETE COMMENT</title>
	</head>
	<body>
	  <p>Delete this comment?</p>
	  <form action="deletecomment.php?<?php echo 'comment_id='.$comment_id ?>" method="POST">
		<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
        <button type="submit" name="delete">Yes</button>
	  </form>
	</body>
  </html>